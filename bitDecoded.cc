//////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Ying-Chun Lai                                             //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
//////////////////////////////////////////////////////////////////////////////

#include "stdio.h"
#include "stdlib.h"
#include <string>
#include <sstream>
#include <iomanip>
#include <set>
#include <vector>
#include <algorithm>

using namespace std;

const int MAX_NR_OF_BITS = 64;
const uint8_t BIT_32 = 32;

vector<uint32_t> getFields(int argc, char **argv)
{
    set<uint32_t> s {0, MAX_NR_OF_BITS};
    if (argc > 2)
    {
        for (int i = 2; i< argc; ++i)
        {
            s.insert(strtoul(argv[i], NULL, 0));
        }
    }
    else
    {
        for (uint8_t i = 0; i < MAX_NR_OF_BITS; i+=4)
        {
            s.insert(i);
        }
    }

    vector<uint32_t> v (s.begin(), s.end());

    return v;
}

void printResult(const uint64_t val, const vector<uint32_t>& fields, const vector<uint64_t>& bitField)
{
    string bit, base2, base10;

    const unsigned int WHITESPC_PER_BIT = 3;
    unsigned int nrOfWhitespace = WHITESPC_PER_BIT; // WHITESPC_PER_BIT * nrOfBits

    uint8_t n = 0;
    for (unsigned int i = 0; i < MAX_NR_OF_BITS ; ++i)
    {
        ostringstream s, s2, s10;
        s << setw(WHITESPC_PER_BIT) << i;

        string bit2 = (val & (1ULL << i)) ? "1":"0";

        if (find(fields.begin(), fields.end(), i + 1) != fields.end())
        {
            s2 << "|" << setw(WHITESPC_PER_BIT-1)<< bit2;
            s10 << "|" << setw(nrOfWhitespace-1) << bitField[n];
            nrOfWhitespace = WHITESPC_PER_BIT;
            ++n;
        }
        else
        {
            s2 << setw(WHITESPC_PER_BIT)<< bit2;
            if (i % BIT_32 == (BIT_32 - 1))
            {
                s10 << setw(nrOfWhitespace)<< "";
                nrOfWhitespace = WHITESPC_PER_BIT;
            }
            else
            {
                nrOfWhitespace += WHITESPC_PER_BIT;
            }
        }

        bit.insert(0, s.str());
        base2.insert(0, s2.str());
        base10.insert(0, s10.str());

        if (i % BIT_32 == (BIT_32 - 1))
        {
            bit.insert(0, "bit\t");
            base2.insert(0, "base2\t");
            base10.insert(0, "base10\t");
            printf("%s\n%s\n%s\n", bit.c_str(), base2.c_str(), base10.c_str());

            bit.clear();
            base2.clear();
            base10.clear();
            nrOfWhitespace = WHITESPC_PER_BIT;
        }
    }

}

int main (int argc, char **argv)
{
    if (argc == 1)
    {
        printf("Missing variable to be parsed.\n");
        printf("Usage: bitDecoded <value> [bit0] [bit1] [bit2] ..\n");
        return 0;
    }

    // TODO: add the "help, -h" support
    // get the value to decode
    string valStr(argv[1]);
    int base = 0;

    if (valStr.find("0b") != string::npos)
    {
        base = 2;
        valStr.erase(0, 2);
    }

    // TODO:check the invalid argument
    uint64_t val = strtoull(valStr.c_str(), NULL, base);

    printf("Value = 0x%lx, %llu (%lld)\n", val, val, val);

    vector<uint32_t> fields = getFields(argc, argv);

    vector<uint64_t> bitFields;
    for (vector<uint32_t>::iterator it = fields.begin();
         *it != MAX_NR_OF_BITS; ++it)
    {
        uint64_t shift = *it;
        uint64_t mask = ((1ULL << (*(it+1) - *it)) -1) << shift;

        //printf("%u - %u = 0x%llx\tmask =0x%lx\n", *it, *(it+1) -1, (val & mask) >> shift, mask);
        bitFields.push_back((val & mask) >> shift);
    }

    //print the formated strings.
    printResult(val, fields, bitFields);

    return 0;
}
