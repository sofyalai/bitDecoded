.PHONY: bitDecoded
bitDecoded:
	g++ -o $@ $@.cc

.PHONY: clean
clean:
	rm -rf *.o bitDecoded
